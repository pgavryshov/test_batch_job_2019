# test_batch_job_2019

Implement a batch job that will perform the following actions:

 1. Read the input .csv file with the data from the folder.  For simplicity, the file name is hard coded: input.csv.  The folder with the input file is configured by the parameter input.dir from the file application.properties
 Input file format:
 unixTimestamp (long); value (int); someField (string)
 Example:
 1530100000; 1000; some_string
 The file can be multi-line, each line corresponds to a separate entry.
 The default delimiter is a semicolon.  Must be configured via the input.delimiter parameter

 Output file format
 unixTimestamp from the input file + 1 day (long); value (from the input file, no change) (int)
 Example:
 1530186400; 1000

 3. Write the result to a file.  The file must be 1) uploaded to the ftp server (lifted locally) 2) copied to the directory specified by the parameter output.dir

 The batch job should end with the following ExitStatus:
 ExitStatus.COMPLETED, if successfully processed, downloaded and copied file
 ExitStatus.FAILED for errors (read, inaccessible ftp, etc.)
 ExitStatus.NOOP, if the input file is not in the specified directory

 When implementing use Spring batch.  To use FTP, use any ready-made library, for example, from apache commons